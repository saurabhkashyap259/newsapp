package com.mobile.newsapp.data.repository;

import android.util.Log;

import com.mobile.newsapp.data.models.weather.WeatherCity;
import com.mobile.newsapp.data.models.weather.WeatherGroup;
import com.mobile.newsapp.data.network.WeatherServices;

import javax.inject.Inject;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WeatherRepository {

    @Inject
    protected WeatherServices weatherServices;

    @Inject
    WeatherRepository() {}

    public LiveData<WeatherGroup> getWeatherGroupList(String ids, String units, String appId) {
        final MutableLiveData<WeatherGroup> weatherGroupMutableLiveData = new MutableLiveData<>();

        weatherServices.getWeatherGroupList(ids, units, appId).enqueue(new Callback<WeatherGroup>() {
            @Override
            public void onResponse(Call<WeatherGroup> call, Response<WeatherGroup> response) {
                //Log.e("WeatherServices", "Response: " + response);
                weatherGroupMutableLiveData.setValue(response.body());
            }

            @Override
            public void onFailure(Call<WeatherGroup> call, Throwable t) {
                //Log.e("WeatherServices", "ResponseError: " + t.getMessage());
                weatherGroupMutableLiveData.setValue(null);
            }
        });

        return weatherGroupMutableLiveData;
    }

    public LiveData<WeatherCity> getWeatherByLatLng(String latitude, String longitude, String appId) {
        final MutableLiveData<WeatherCity> weatherCityMutableLiveData = new MutableLiveData<>();

        weatherServices.getWeatherByLatLng(latitude, longitude, appId).enqueue(new Callback<WeatherCity>() {
            @Override
            public void onResponse(Call<WeatherCity> call, Response<WeatherCity> response) {
                Log.e("WeatherServices", "Response: " + response);
                weatherCityMutableLiveData.setValue(response.body());
            }

            @Override
            public void onFailure(Call<WeatherCity> call, Throwable t) {
                Log.e("WeatherServices", "ResponseError: " + t.getMessage());
                weatherCityMutableLiveData.setValue(null);
            }
        });

        return weatherCityMutableLiveData;
    }
}
