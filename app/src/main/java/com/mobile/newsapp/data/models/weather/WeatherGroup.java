package com.mobile.newsapp.data.models.weather;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class WeatherGroup {

    @SerializedName("cnt")
    @Expose
    private String count;

    @SerializedName("list")
    @Expose
    private List<WeatherCity> weatherCityList;

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public List<WeatherCity> getWeatherCityList() {
        return weatherCityList;
    }

    public void setWeatherCityList(List<WeatherCity> weatherCityList) {
        this.weatherCityList = weatherCityList;
    }
}
