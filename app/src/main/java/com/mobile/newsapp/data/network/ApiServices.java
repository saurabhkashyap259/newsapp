package com.mobile.newsapp.data.network;

import com.mobile.newsapp.data.models.ArticleList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiServices {

    @GET("top-headlines")
    Call<ArticleList> getTopHeadlines(
            @Query("sources") String sources,
            @Query("apiKey") String apiKey
    );

    @GET("top-headlines")
    Call<ArticleList> getNationalNews(
            @Query("country") String country,
            @Query("apiKey") String apiKey
    );
}
