package com.mobile.newsapp.data.repository;

import com.mobile.newsapp.data.models.ArticleList;
import com.mobile.newsapp.data.network.ApiServices;

import javax.inject.Inject;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GoogleNewsRepository {

    @Inject
    protected ApiServices apiServices;

    @Inject
    GoogleNewsRepository() {}

    public LiveData<ArticleList> getTopHeadlines(String sources, String apiKey) {
        final MutableLiveData<ArticleList> articleListMutableLiveData = new MutableLiveData<>();

        apiServices.getTopHeadlines(sources, apiKey).enqueue(new Callback<ArticleList>() {
            @Override
            public void onResponse(Call<ArticleList> call, Response<ArticleList> response) {
                articleListMutableLiveData.setValue(response.body());
            }

            @Override
            public void onFailure(Call<ArticleList> call, Throwable t) {
                articleListMutableLiveData.setValue(null);
            }
        });

        return articleListMutableLiveData;
    }

    public LiveData<ArticleList> getNationalNews(String country, String apiKey) {
        final MutableLiveData<ArticleList> articleListMutableLiveData = new MutableLiveData<>();

        apiServices.getNationalNews(country, apiKey).enqueue(new Callback<ArticleList>() {
            @Override
            public void onResponse(Call<ArticleList> call, Response<ArticleList> response) {
                articleListMutableLiveData.setValue(response.body());
            }

            @Override
            public void onFailure(Call<ArticleList> call, Throwable t) {
                articleListMutableLiveData.setValue(null);
            }
        });

        return articleListMutableLiveData;
    }
}
