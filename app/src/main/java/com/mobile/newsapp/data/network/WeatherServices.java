package com.mobile.newsapp.data.network;

import com.mobile.newsapp.data.models.weather.WeatherCity;
import com.mobile.newsapp.data.models.weather.WeatherGroup;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface WeatherServices {

    @GET("group")
    Call<WeatherGroup> getWeatherGroupList(
            @Query("id") String id,
            @Query("units") String units,
            @Query("appid") String appId
    );

    @GET("weather")
    Call<WeatherCity> getWeatherByLatLng(
            @Query("lat") String latitude,
            @Query("lng") String longitude,
            @Query("appid") String appId
    );
}
