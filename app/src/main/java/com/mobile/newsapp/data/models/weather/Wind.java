package com.mobile.newsapp.data.models.weather;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Wind {

    @SerializedName("speed")
    @Expose
    private String speed;

    @SerializedName("deg")
    @Expose
    private String deg;
}
