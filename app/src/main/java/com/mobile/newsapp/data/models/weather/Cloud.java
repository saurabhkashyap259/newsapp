package com.mobile.newsapp.data.models.weather;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Cloud {

    @SerializedName("all")
    @Expose
    private String all;
}
