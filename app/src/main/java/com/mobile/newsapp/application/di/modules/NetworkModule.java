package com.mobile.newsapp.application.di.modules;

import com.mobile.newsapp.data.network.ApiServices;
import com.mobile.newsapp.data.network.WeatherServices;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class NetworkModule {

    private static final String GOOGLE_NEWS_BASE_URL = "https://newsapi.org/v2/";
    private static final String GOOGLE_WEATHER_BASE_URL = "https://api.openweathermap.org/data/2.5/";

    @Provides
    ApiServices providesApiServices() {
        Retrofit retrofit = new Retrofit.Builder()
                .client(new OkHttpClient())
                .baseUrl(GOOGLE_NEWS_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit.create(ApiServices.class);
    }

    @Provides
    WeatherServices providesWeatherServices() {
        Retrofit retrofit = new Retrofit.Builder()
                .client(new OkHttpClient())
                .baseUrl(GOOGLE_WEATHER_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit.create(WeatherServices.class);
    }
}
