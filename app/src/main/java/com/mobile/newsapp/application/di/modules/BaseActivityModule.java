package com.mobile.newsapp.application.di.modules;

import com.mobile.newsapp.application.base.BaseActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class BaseActivityModule {
    @ContributesAndroidInjector(modules = FragmentBuildersModule.class)
    abstract BaseActivity contributeBaseActivity();
}
