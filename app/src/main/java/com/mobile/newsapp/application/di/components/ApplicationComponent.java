package com.mobile.newsapp.application.di.components;

import android.app.Application;

import com.mobile.newsapp.application.base.BaseApplication;
import com.mobile.newsapp.application.di.modules.ApplicationModule;
import com.mobile.newsapp.application.di.modules.BaseActivityModule;
import com.mobile.newsapp.application.di.modules.NetworkModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;

@Singleton
@Component(modules = {AndroidInjectionModule.class, ApplicationModule.class, BaseActivityModule.class, NetworkModule.class})
public interface ApplicationComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance Builder application(Application application);
        ApplicationComponent build();
    }
    void inject(BaseApplication baseApplication);
}
