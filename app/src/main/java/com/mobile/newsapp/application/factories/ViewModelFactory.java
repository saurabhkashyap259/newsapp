package com.mobile.newsapp.application.factories;

import com.mobile.newsapp.application.di.components.ViewModelSubComponent;
import com.mobile.newsapp.modules.news.viewmodels.NationalNewsViewModel;
import com.mobile.newsapp.modules.news.viewmodels.NewsViewModel;
import com.mobile.newsapp.modules.weather.viewmodels.WeatherViewModel;

import java.util.Map;
import java.util.concurrent.Callable;

import javax.inject.Inject;
import javax.inject.Singleton;

import androidx.annotation.NonNull;
import androidx.collection.ArrayMap;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

@Singleton
public class ViewModelFactory implements ViewModelProvider.Factory {
    private final ArrayMap<Class, Callable<? extends ViewModel>> creators;

    @Inject
    public ViewModelFactory(ViewModelSubComponent viewModelSubComponent) {
        creators = new ArrayMap<>();

        // View models cannot be injected directly because they won't be bound to the owner's view model scope.
        creators.put(NewsViewModel.class, viewModelSubComponent::newsViewModel);
        creators.put(NationalNewsViewModel.class, viewModelSubComponent::nationalNewsViewModel);
        creators.put(WeatherViewModel.class, viewModelSubComponent::weatherViewModel);
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        Callable<? extends ViewModel> creator = creators.get(modelClass);
        if (creator == null) {
            for (Map.Entry<Class, Callable<? extends ViewModel>> entry : creators.entrySet()) {
                if (modelClass.isAssignableFrom(entry.getKey())) {
                    creator = entry.getValue();
                    break;
                }
            }
        }
        if (creator == null) {
            throw new IllegalArgumentException("Unknown model class " + modelClass);
        }
        try {
            return (T) creator.call();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
