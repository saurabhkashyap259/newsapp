package com.mobile.newsapp.application.di.modules;

import com.mobile.newsapp.application.di.components.ViewModelSubComponent;
import com.mobile.newsapp.application.factories.ViewModelFactory;

import javax.inject.Singleton;

import androidx.lifecycle.ViewModelProvider;
import dagger.Module;
import dagger.Provides;

@Module(subcomponents = ViewModelSubComponent.class)
public class ApplicationModule {

    @Singleton
    @Provides
    ViewModelProvider.Factory viewModelFactory(
            ViewModelSubComponent.Builder viewModelSubComponent) {

        return new ViewModelFactory(viewModelSubComponent.build());
    }
}
