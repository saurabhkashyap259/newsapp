package com.mobile.newsapp.application.di.components;

import com.mobile.newsapp.modules.news.viewmodels.NationalNewsViewModel;
import com.mobile.newsapp.modules.news.viewmodels.NewsViewModel;
import com.mobile.newsapp.modules.weather.viewmodels.WeatherViewModel;

import dagger.Subcomponent;

@Subcomponent
public interface ViewModelSubComponent {
    @Subcomponent.Builder
    interface Builder {
        ViewModelSubComponent build();
    }

    NewsViewModel newsViewModel();
    NationalNewsViewModel nationalNewsViewModel();
    WeatherViewModel weatherViewModel();
}
