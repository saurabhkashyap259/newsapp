package com.mobile.newsapp.application.di.modules;

import com.mobile.newsapp.modules.home.HomeFragment;
import com.mobile.newsapp.modules.news.fragments.FavoriteFragment;
import com.mobile.newsapp.modules.news.fragments.InternationalNewsFragment;
import com.mobile.newsapp.modules.news.fragments.NationalNewsFragment;
import com.mobile.newsapp.modules.news.fragments.NewsFragment;
import com.mobile.newsapp.modules.news.fragments.SportsNewsFragment;
import com.mobile.newsapp.modules.weather.WeatherFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class FragmentBuildersModule {

    @ContributesAndroidInjector
    abstract HomeFragment contributesHomeFragment();

    @ContributesAndroidInjector
    abstract NewsFragment contributesNewsFragment();

    @ContributesAndroidInjector
    abstract FavoriteFragment contributesFavoriteFragment();

    @ContributesAndroidInjector
    abstract NationalNewsFragment contributesNationalNewsFragment();

    @ContributesAndroidInjector
    abstract WeatherFragment contributesWeatherFragment();

    @ContributesAndroidInjector
    abstract InternationalNewsFragment contributesInternationalNewsFragment();

    @ContributesAndroidInjector
    abstract SportsNewsFragment contributesSportsNewsFragment();
}
