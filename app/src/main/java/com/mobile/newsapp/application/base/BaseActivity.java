package com.mobile.newsapp.application.base;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;

import com.google.android.material.navigation.NavigationView;
import com.mobile.newsapp.R;
import com.mobile.newsapp.application.base.adapters.ExpandableListAdapter;
import com.mobile.newsapp.application.base.models.MenuModel;
import com.mobile.newsapp.modules.home.HomeFragment;
import com.mobile.newsapp.modules.news.fragments.InternationalNewsFragment;
import com.mobile.newsapp.modules.news.fragments.NationalNewsFragment;
import com.mobile.newsapp.modules.news.fragments.SportsNewsFragment;
import com.mobile.newsapp.modules.weather.WeatherFragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;

public class BaseActivity extends AppCompatActivity implements HasSupportFragmentInjector, NavigationView.OnNavigationItemSelectedListener {

    private ExpandableListAdapter expandableListAdapter;
    private ExpandableListView expandableListView;
    private List<MenuModel> headerList = new ArrayList<>();
    private HashMap<MenuModel, List<MenuModel>> childList = new HashMap<>();

    @Inject
    DispatchingAndroidInjector<Fragment> dispatchingAndroidInjector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        expandableListView = findViewById(R.id.expandableListView);
        prepareMenuData();
        populateExpandableList();

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        openHomeFragment();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void openHomeFragment() {
        getSupportFragmentManager().beginTransaction()
                .add(R.id.fragment_container, new HomeFragment(), HomeFragment.TAG).commit();
    }

    private void openNationalNewsFragment() {
        getSupportFragmentManager().beginTransaction()
                .add(R.id.fragment_container, new NationalNewsFragment(), NationalNewsFragment.TAG).commit();
    }

    private void openInternationalNewsFragment() {
        getSupportFragmentManager().beginTransaction()
                .add(R.id.fragment_container, new InternationalNewsFragment(), InternationalNewsFragment.TAG).commit();
    }

    private void openSportsNewsFragment() {
        getSupportFragmentManager().beginTransaction()
                .add(R.id.fragment_container, new SportsNewsFragment(), InternationalNewsFragment.TAG).commit();
    }

    private void openWeatherFragment() {
        getSupportFragmentManager().beginTransaction()
                .add(R.id.fragment_container, new WeatherFragment(), HomeFragment.TAG).commit();
    }

    @Override
    public DispatchingAndroidInjector<Fragment> supportFragmentInjector() {
        return dispatchingAndroidInjector;
    }

    private void prepareMenuData() {

        MenuModel menuModel = new MenuModel("News", true, true);
        headerList.add(menuModel);

        List<MenuModel> childModelsList = new ArrayList<>();
        MenuModel childModel = new MenuModel("National", false, false);
        childModelsList.add(childModel);

        childModel = new MenuModel("International", false, false);
        childModelsList.add(childModel);

        childModel = new MenuModel("Sports", false, false);
        childModelsList.add(childModel);

        childModel = new MenuModel("Latest", false, false);
        childModelsList.add(childModel);

        if (menuModel.isHasChildren()) {
            Log.d("API123","here");
            childList.put(menuModel, childModelsList);
        }

        menuModel = new MenuModel("Weather", true, false);
        headerList.add(menuModel);

        if (!menuModel.isHasChildren()) {
            childList.put(menuModel, null);
        }
    }

    private void populateExpandableList() {

        expandableListAdapter = new ExpandableListAdapter(this, headerList, childList);
        expandableListView.setAdapter(expandableListAdapter);

        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {

                if (headerList.get(groupPosition).isGroup()) {
                    if (!headerList.get(groupPosition).isHasChildren()) {
                        openFragments(headerList.get(groupPosition).getMenuName());
                    }
                }

                return false;
            }
        });

        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {

                if (childList.get(headerList.get(groupPosition)) != null) {
                    MenuModel model = childList.get(headerList.get(groupPosition)).get(childPosition);
                    openFragments(model.getMenuName());
                }

                return false;
            }
        });
    }

    private void openFragments(String name) {
        switch (name) {
            case "National":
                openNationalNewsFragment();
                break;

            case "International":
                openInternationalNewsFragment();
                break;

            case "Sports":
                openSportsNewsFragment();
                break;

            case "Latest":
                openHomeFragment();
                break;

            case "Weather":
                openWeatherFragment();
                break;

        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        return false;
    }
}
