package com.mobile.newsapp.modules.news.adapters.viewholders;

import android.content.Context;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mobile.newsapp.R;
import com.mobile.newsapp.data.models.Article;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class NationalNewsItemViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.item_home_iv)
    ImageView imageView;
    @BindView(R.id.item_home_title_tv)
    TextView titleTV;
    @BindView(R.id.item_home_description_tv)
    TextView descriptionTV;
    @BindView(R.id.item_home_bookmark_cb)
    CheckBox bookmarkCB;
    private Context context;

    public NationalNewsItemViewHolder(@NonNull View itemView, Context context) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.context = context;
    }

    public void setData(Article article) {
        if(article != null) {
            bookmarkCB.setVisibility(View.GONE);
            Glide.with(context).load(article.getUrlToImage()).into(imageView);
            titleTV.setText(article.getTitle());
            descriptionTV.setText(article.getDescription());
        }
    }
}
