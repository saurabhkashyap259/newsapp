package com.mobile.newsapp.modules.news.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mobile.newsapp.R;
import com.mobile.newsapp.data.models.Article;
import com.mobile.newsapp.modules.news.adapters.viewholders.NewsItemViewHolder;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class NewsAdapter extends RecyclerView.Adapter<NewsItemViewHolder> {

    private Context context;
    private List<Article> articleList = new ArrayList<>();
    private InterfaceNewsAdapter interfaceNewsAdapter;

    public NewsAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public NewsItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_news, parent, false);

        return new NewsItemViewHolder(itemView, context);
    }

    @Override
    public void onBindViewHolder(@NonNull NewsItemViewHolder holder, int position) {
        holder.setData(articleList.get(position), interfaceNewsAdapter, position);
    }

    @Override
    public int getItemCount() {
        return articleList.size();
    }

    public void setArticleList(List<Article> articleList) {
        this.articleList.clear();
        this.articleList.addAll(articleList);
        notifyDataSetChanged();
    }

    public List<Article> getArticleList() {
        return articleList;
    }

    public interface InterfaceNewsAdapter {
        void onBookmarkClick(int position, Article article);
    }

    public void setInterfaceNewsAdapter(InterfaceNewsAdapter listener) {
        this.interfaceNewsAdapter = listener;
    }
}
