package com.mobile.newsapp.modules.news.viewmodels;

import android.app.Application;

import com.mobile.newsapp.data.models.Article;
import com.mobile.newsapp.data.models.ArticleList;
import com.mobile.newsapp.data.repository.GoogleNewsRepository;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

public class NewsViewModel extends AndroidViewModel {

    private LiveData<ArticleList> articleListLiveData;
    private MutableLiveData<String> source;
    private GoogleNewsRepository googleNewsRepository;

    @Inject
    public NewsViewModel(@NonNull GoogleNewsRepository googleNewsRepository, @NonNull Application application) {
        super(application);
        source = new MutableLiveData<>();
        this.googleNewsRepository = googleNewsRepository;
        articleListLiveData = googleNewsRepository.getTopHeadlines("google-news", "cf12f5bc65c14213a0189c503e164d97");
    }

    public LiveData<ArticleList> getArticleListLiveData() {
        return articleListLiveData;
    }

    public LiveData<ArticleList> getSportsArticleListLiveData() {
        return googleNewsRepository.getTopHeadlines("bbc-sport", "cf12f5bc65c14213a0189c503e164d97");
    }
}
