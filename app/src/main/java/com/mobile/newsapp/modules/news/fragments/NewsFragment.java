package com.mobile.newsapp.modules.news.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mobile.newsapp.R;
import com.mobile.newsapp.application.di.injectors.Injectable;
import com.mobile.newsapp.data.models.Article;
import com.mobile.newsapp.data.models.ArticleList;
import com.mobile.newsapp.modules.news.adapters.NewsAdapter;
import com.mobile.newsapp.modules.news.viewmodels.NewsViewModel;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class NewsFragment extends Fragment implements Injectable {

    public static final String TAG = NewsFragment.class.getSimpleName();

    @BindView(R.id.fragment_news_rv)
    RecyclerView recyclerView;
    private Context context;
    private NewsAdapter adapter;
    private NewsViewModel viewModel;
    private InterfaceNewsFragment interfaceNewsFragment;

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getContext();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_news, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        adapter = new NewsAdapter(context);
        recyclerView.setAdapter(adapter);

        adapter.setInterfaceNewsAdapter(new NewsAdapter.InterfaceNewsAdapter() {
            @Override
            public void onBookmarkClick(int position, Article article) {
                interfaceNewsFragment.onBookmarkClick(article);
            }
        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(NewsViewModel.class);
        observeViewModel(viewModel);
    }

    private void observeViewModel(final NewsViewModel viewModel) {
        // Observe project data
        viewModel.getArticleListLiveData().observe(this, new Observer<ArticleList>() {
            @Override
            public void onChanged(@Nullable ArticleList articleList) {
                if(articleList != null) {
                    adapter.setArticleList(articleList.getArticleList());
                }
            }
        });
    }

    public interface InterfaceNewsFragment {
        void onBookmarkClick(Article article);
    }

    public void setInterfaceNewsFragment(InterfaceNewsFragment listener) {
        this.interfaceNewsFragment = listener;
    }
}
