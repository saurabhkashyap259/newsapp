package com.mobile.newsapp.modules.weather.viewmodels;

import android.app.Application;

import com.mobile.newsapp.data.models.weather.WeatherCity;
import com.mobile.newsapp.data.models.weather.WeatherGroup;
import com.mobile.newsapp.data.repository.WeatherRepository;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

public class WeatherViewModel extends AndroidViewModel {

    private LiveData<WeatherGroup> weatherGroupLiveData;
    private MutableLiveData<String> cityIds;
    private MutableLiveData<String> units;
    private MutableLiveData<String> latitude;
    private MutableLiveData<String> longitude;
    private WeatherRepository weatherRepository;

    @Inject
    public WeatherViewModel(@NonNull WeatherRepository weatherRepository, @NonNull Application application) {
        super(application);
        this.weatherRepository = weatherRepository;
        cityIds = new MutableLiveData<>();
        units = new MutableLiveData<>();
        longitude = new MutableLiveData<>();
        latitude = new MutableLiveData<>();
    }

    public void setWeatherGroupLiveData() {
        weatherGroupLiveData = weatherRepository.getWeatherGroupList(cityIds.getValue(), units.getValue(), "7ded24d624ebd117dd4ef3e8bbeef4bd");
    }

    public LiveData<WeatherGroup> getWeatherGroupLiveData() {
        return weatherGroupLiveData;
    }

    public LiveData<WeatherCity> getWeatherCityLiveData() {
        return weatherRepository.getWeatherByLatLng(latitude.getValue(), longitude.getValue(), "7ded24d624ebd117dd4ef3e8bbeef4bd");
    }

    public void setCityIds(String cityIds) {
        this.cityIds.setValue(cityIds);
    }

    public void setUnits(String units) {
        this.units.setValue(units);
    }

    public void setLatitude(String latitude) {
        this.latitude.setValue(latitude);
    }

    public void setLongitude(String longitude) {
        this.longitude.setValue(longitude);
    }
}
