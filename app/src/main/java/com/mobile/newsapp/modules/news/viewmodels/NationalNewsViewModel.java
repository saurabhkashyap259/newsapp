package com.mobile.newsapp.modules.news.viewmodels;

import android.app.Application;

import com.mobile.newsapp.data.models.ArticleList;
import com.mobile.newsapp.data.repository.GoogleNewsRepository;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

public class NationalNewsViewModel extends AndroidViewModel {

    private LiveData<ArticleList> articleListLiveData;

    @Inject
    public NationalNewsViewModel(@NonNull GoogleNewsRepository googleNewsRepository, @NonNull Application application) {
        super(application);
        articleListLiveData = googleNewsRepository.getNationalNews("in", "cf12f5bc65c14213a0189c503e164d97");
    }

    public LiveData<ArticleList> getArticleListLiveData() {
        return articleListLiveData;
    }
}
