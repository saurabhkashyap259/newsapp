package com.mobile.newsapp.modules.news.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mobile.newsapp.R;
import com.mobile.newsapp.data.models.Article;
import com.mobile.newsapp.modules.news.adapters.viewholders.NationalNewsItemViewHolder;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class NationalNewsAdapter extends RecyclerView.Adapter<NationalNewsItemViewHolder> {

    private Context context;
    private List<Article> articleList = new ArrayList<>();

    public NationalNewsAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public NationalNewsItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_news, parent, false);

        return new NationalNewsItemViewHolder(itemView, context);
    }

    @Override
    public void onBindViewHolder(@NonNull NationalNewsItemViewHolder holder, int position) {
        holder.setData(articleList.get(position));
    }

    @Override
    public int getItemCount() {
        return articleList.size();
    }

    public void setArticleList(List<Article> articleList) {
        this.articleList.clear();
        this.articleList.addAll(articleList);
        notifyDataSetChanged();
    }
}
