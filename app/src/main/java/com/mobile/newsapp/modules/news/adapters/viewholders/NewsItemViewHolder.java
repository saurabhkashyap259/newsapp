package com.mobile.newsapp.modules.news.adapters.viewholders;

import android.content.Context;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mobile.newsapp.R;
import com.mobile.newsapp.data.models.Article;
import com.mobile.newsapp.modules.news.adapters.NewsAdapter;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class NewsItemViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.item_home_iv)
    ImageView imageView;
    @BindView(R.id.item_home_title_tv)
    TextView titleTV;
    @BindView(R.id.item_home_description_tv)
    TextView descriptionTV;
    @BindView(R.id.item_news_root_cv)
    CardView cardView;
    @BindView(R.id.item_home_bookmark_cb)
    CheckBox bookmarkCB;
    private Context context;

    public NewsItemViewHolder(@NonNull View itemView, Context context) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.context = context;
    }

    public void setData(Article article, NewsAdapter.InterfaceNewsAdapter interfaceNewsAdapter, int position) {
        if(article != null) {
            Glide.with(context).load(article.getUrlToImage()).into(imageView);
            titleTV.setText(article.getTitle());
            descriptionTV.setText(article.getDescription());

            if(article.isFavourite()) {
                bookmarkCB.setChecked(true);
                bookmarkCB.setEnabled(false);
            }else {
                bookmarkCB.setChecked(false);
                bookmarkCB.setEnabled(true);
            }

            bookmarkCB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(isChecked) {
                        article.setFavourite(true);
                        bookmarkCB.setEnabled(false);
                        interfaceNewsAdapter.onBookmarkClick(position, article);
                    }
                }
            });
        }
    }
}
