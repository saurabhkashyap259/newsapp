package com.mobile.newsapp.modules.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.tabs.TabLayout;
import com.mobile.newsapp.R;
import com.mobile.newsapp.application.di.injectors.Injectable;
import com.mobile.newsapp.data.models.Article;
import com.mobile.newsapp.modules.home.adapters.HomePagerAdapter;
import com.mobile.newsapp.modules.news.fragments.FavoriteFragment;
import com.mobile.newsapp.modules.news.fragments.NewsFragment;
import com.mobile.newsapp.modules.news.viewmodels.NewsViewModel;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeFragment extends Fragment implements Injectable, NewsFragment.InterfaceNewsFragment {

    public static final String TAG = HomeFragment.class.getSimpleName();

    @BindView(R.id.fragment_home_tl)
    TabLayout tabLayout;
    @BindView(R.id.fragment_home_vp)
    ViewPager viewPager;
    private HomePagerAdapter adapter;
    private NewsFragment newsFragment;
    private FavoriteFragment favoriteFragment;

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        tabLayout.setupWithViewPager(viewPager);
        adapter = new HomePagerAdapter(getChildFragmentManager());
        viewPager.setOffscreenPageLimit(0);

        setUpViewPager();

    }

    private void setUpViewPager() {
        if (viewPager != null) {
            adapter.addFragment(getNewsFragment(), "News");
            adapter.addFragment(getFavoriteFragment(), "Favorites");
            viewPager.setAdapter(adapter);
        }
    }

    private NewsFragment getNewsFragment() {
        newsFragment = new NewsFragment();
        return newsFragment;
    }

    private FavoriteFragment getFavoriteFragment() {
        favoriteFragment = new FavoriteFragment();
        return favoriteFragment;
    }

    @Override
    public void onAttachFragment(Fragment childFragment) {
        super.onAttachFragment(childFragment);
        if(childFragment instanceof NewsFragment) {
            NewsFragment newsFragment = (NewsFragment) childFragment;
            newsFragment.setInterfaceNewsFragment(this);
        }
    }

    @Override
    public void onBookmarkClick(Article article) {
        favoriteFragment.addArticle(article);
    }
}
