package com.mobile.newsapp.modules.weather;

import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.PlaceDetectionClient;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.mobile.newsapp.R;
import com.mobile.newsapp.application.di.injectors.Injectable;
import com.mobile.newsapp.data.models.weather.WeatherCity;
import com.mobile.newsapp.data.models.weather.WeatherGroup;
import com.mobile.newsapp.modules.weather.viewmodels.WeatherViewModel;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

public class WeatherFragment extends Fragment implements Injectable, OnMapReadyCallback {

    private static final String TAG = WeatherFragment.class.getSimpleName();
    private Context context;
    private WeatherViewModel viewModel;
    private WeatherGroup weatherGroup;
    private GoogleMap googleMap;

    private PlaceDetectionClient mPlaceDetectionClient;
    private FusedLocationProviderClient mFusedLocationProviderClient;

    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    private boolean mLocationPermissionGranted;

    private Location mLastKnownLocation;

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getContext();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //ButterKnife.bind(this, view);
        return inflater.inflate(R.layout.fragment_weather, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Construct a PlaceDetectionClient.
        mPlaceDetectionClient = Places.getPlaceDetectionClient(getContext(), null);

        // Construct a FusedLocationProviderClient.
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getContext());

        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(WeatherViewModel.class);
        viewModel.setCityIds(getCityIds());
        viewModel.setUnits(getUnits());
        viewModel.setWeatherGroupLiveData();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        getLocationPermission();
        updateLocationUI();
        getDeviceLocation();
       observeViewModel(googleMap);
    }

    private void getDeviceLocation() {
        /*
         * Get the best and most recent location of the device, which may be null in rare
         * cases when a location is not available.
         */
        try {
            if (mLocationPermissionGranted) {
                Task<Location> locationResult = mFusedLocationProviderClient.getLastLocation();
                locationResult.addOnCompleteListener(getActivity(), new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        if (task.isSuccessful()) {
                            // Set the map's camera position to the current location of the device.
                            mLastKnownLocation = task.getResult();
                            getWeatherAtLatLng();
                        } else {
                            /*Log.d(TAG, "Current location is null. Using defaults.");
                            Log.e(TAG, "Exception: %s", task.getException());
                            googleMap.moveCamera(CameraUpdateFactory
                                    .newLatLngZoom(mDefaultLocation, DEFAULT_ZOOM));
                            googleMap.getUiSettings().setMyLocationButtonEnabled(false);*/
                        }
                    }
                });
            }
        } catch (SecurityException e)  {
            Log.e("Exception: %s", e.getMessage());
        }
    }

    private void updateLocationUI() {
        if (googleMap == null) {
            return;
        }
        try {
            if (mLocationPermissionGranted) {
                googleMap.setMyLocationEnabled(true);
                //googleMap.getUiSettings().setMyLocationButtonEnabled(true);
            } else {
                googleMap.setMyLocationEnabled(false);
                googleMap.getUiSettings().setMyLocationButtonEnabled(false);
                mLastKnownLocation = null;
                getLocationPermission();
            }
        } catch (SecurityException e)  {
            Log.e("Exception: %s", e.getMessage());
        }
    }

    private void getLocationPermission() {
        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         * onRequestPermissionsResult.
         */
        if (ContextCompat.checkSelfPermission(getContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true;
        } else {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
    }

    private void observeViewModel(GoogleMap googleMap) {
        viewModel.getWeatherGroupLiveData().observe(this, weatherGroup -> {
            if(weatherGroup != null) {
                addMarkers(googleMap, weatherGroup);
            }
        });
    }

    private void getWeatherAtLatLng() {
        viewModel.setLatitude(String.valueOf(mLastKnownLocation.getLatitude()));
        viewModel.setLongitude(String.valueOf(mLastKnownLocation.getLongitude()));
        viewModel.getWeatherCityLiveData().observe(this, new Observer<WeatherCity>() {
            @Override
            public void onChanged(WeatherCity weatherCity) {
                if(weatherCity != null) {
                    googleMap.addMarker(new MarkerOptions().position(new LatLng(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude()))
                            .title(weatherCity.getName())
                            .snippet(weatherCity.getWeatherList().get(0).getDescription())
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)));
                }
            }
        });
    }

    private void addMarkers(GoogleMap googleMap, WeatherGroup weatherGroup) {
        LatLng delhi = new LatLng(28.644800, 77.216721);
        googleMap.addMarker(new MarkerOptions().position(delhi)
                .title(weatherGroup.getWeatherCityList().get(0).getName())
                .snippet(weatherGroup.getWeatherCityList().get(0).getWeatherList().get(0).getDescription())
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(delhi));

        LatLng kolkata = new LatLng(22.572645, 88.363892);
        googleMap.addMarker(new MarkerOptions().position(kolkata)
                .title(weatherGroup.getWeatherCityList().get(1).getName())
                .snippet(weatherGroup.getWeatherCityList().get(1).getWeatherList().get(0).getDescription())
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(kolkata));

        LatLng mumbai = new LatLng(19.228825, 72.854118);
        googleMap.addMarker(new MarkerOptions().position(mumbai)
                .title(weatherGroup.getWeatherCityList().get(2).getName())
                .snippet(weatherGroup.getWeatherCityList().get(2).getWeatherList().get(0).getDescription())
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(mumbai));

        LatLng chennai = new LatLng(13.067439, 80.237617);
        googleMap.addMarker(new MarkerOptions().position(chennai)
                .title(weatherGroup.getWeatherCityList().get(3).getName())
                .snippet(weatherGroup.getWeatherCityList().get(3).getWeatherList().get(0).getDescription())
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(chennai));
    }

    private String getCityIds() {
        return "1273294,1275004,6619347,1264527";
    }

    private String getUnits() {
        return "metrics";
    }
}
